package com;


import org.junit.Test;
import static org.junit.Assert.assertEquals;
import com.GraphRelated.Graph;

/**
 * Unit test for simple App.
 */
public class AppTest{
public void testAddVertex() {
    Graph graph = new Graph(5);
    graph.addVertex("A");
    graph.addVertex("B");
    graph.addVertex("C");
    assertEquals(3, graph.getVerticesCount());
}

@Test
public void testAddEdge() {
    Graph graph = new Graph(5);
    graph.addEdge("A", "B", 10);
    graph.addEdge("B", "C", 5);
    assertEquals(10, graph.getEdgeWeight("A", "B"));
    assertEquals(5, graph.getEdgeWeight("B", "C"));
}

@Test
public void testGetEdgeWeight() {
    Graph graph = new Graph(5);
    graph.addEdge("A", "B", 10);
    graph.addEdge("B", "C", 5);
    assertEquals(10, graph.getEdgeWeight("A", "B"));
    assertEquals(5, graph.getEdgeWeight("B", "C"));
    assertEquals(Integer.MAX_VALUE, graph.getEdgeWeight("A", "C"));
}

@Test
public void testGetVertex() {
    Graph graph = new Graph(5);
    graph.addVertex("A");
    graph.addVertex("B");
    graph.addVertex("C");
    assertEquals("A", graph.getVertex(0));
    assertEquals("B", graph.getVertex(1));
    assertEquals("C", graph.getVertex(2));
}

@Test
public void testGetVertexIndex() {
    Graph graph = new Graph(5);
    graph.addVertex("A");
    graph.addVertex("B");
    graph.addVertex("C");
    assertEquals(0, graph.getVertexIndex("A"));
    assertEquals(1, graph.getVertexIndex("B"));
    assertEquals(2, graph.getVertexIndex("C"));
    assertEquals(-1, graph.getVertexIndex("D"));
}
}