package com.GraphRelated;

public class GraphManager{
    // Nah, Djikstra better, ngl
    public static void Floyd(Graph graph, String source, String destination){
        int verticesCount = graph.getVerticesCount();
        int[][] distance = new int[verticesCount][verticesCount];
        int[][] next = new int[verticesCount][verticesCount];

        for (int i = 0; i < verticesCount; i++) {
            for (int j = 0; j < verticesCount; j++) {
                distance[i][j] = graph.getEdgeWeight(graph.getVertex(i), graph.getVertex(j));
                next[i][j] = j;
            }
        }

        for (int k = 0; k < verticesCount; k++) {
            for (int i = 0; i < verticesCount; i++) {
                for (int j = 0; j < verticesCount; j++) {
                    if (distance[i][k] == Integer.MAX_VALUE || distance[k][j] == Integer.MAX_VALUE) {
                        continue;
                    }
                    if (distance[i][j] > distance[i][k] + distance[k][j]) {
                        distance[i][j] = distance[i][k] + distance[k][j];
                        next[i][j] = next[i][k];
                    }
                }
            }
        }

        int sourceIndex = graph.getVertexIndex(source);
        int destinationIndex = graph.getVertexIndex(destination);
        if (distance[sourceIndex][destinationIndex] == Integer.MAX_VALUE) {
            System.out.println("No hay Camino entre " + source + " y " + destination);
            return;
        }

        StringBuilder path = new StringBuilder();
        path.append(source);
        while (sourceIndex != destinationIndex) {
            sourceIndex = next[sourceIndex][destinationIndex];
            path.append(" -> ").append(graph.getVertex(sourceIndex));
        }
        System.out.println("Camino mas corto " + path);
    }

    // Cool algorithm, might never use it again
    public static void Center(Graph graph){
        int verticesCount = graph.getVerticesCount();
        int[][] distance = new int[verticesCount][verticesCount];
        int[][] next = new int[verticesCount][verticesCount];

        for (int i = 0; i < verticesCount; i++) {
            for (int j = 0; j < verticesCount; j++) {
                distance[i][j] = graph.getEdgeWeight(graph.getVertex(i), graph.getVertex(j));
                next[i][j] = j;
            }
        }

        for (int k = 0; k < verticesCount; k++) {
            for (int i = 0; i < verticesCount; i++) {
                for (int j = 0; j < verticesCount; j++) {
                    if (distance[i][k] == Integer.MAX_VALUE || distance[k][j] == Integer.MAX_VALUE) {
                        continue;
                    }
                    if (distance[i][j] > distance[i][k] + distance[k][j]) {
                        distance[i][j] = distance[i][k] + distance[k][j];
                        next[i][j] = next[i][k];
                    }
                }
            }
        }

        int minMaxDistance = Integer.MAX_VALUE;
        int center = -1;
        for (int i = 0; i < verticesCount; i++) {
            int maxDistance = 0;
            for (int j = 0; j < verticesCount; j++) {
                if (distance[i][j] > maxDistance) {
                    maxDistance = distance[i][j];
                }
            }
            if (maxDistance < minMaxDistance) {
                minMaxDistance = maxDistance;
                center = i;
            }
        }

        System.out.println("Centro del Grafo: " + graph.getVertex(center));
    }
}