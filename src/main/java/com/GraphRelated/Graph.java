package com.GraphRelated;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


public class Graph {
    private final HashMap<String, Integer> vertexIndexMap;
    private final ArrayList<String> vertices;
    private final int[][] adjacencyMatrix;

    public Graph(int maxVertices) {
        this.vertexIndexMap = new HashMap<>();
        this.vertices = new ArrayList<>();
        this.adjacencyMatrix = new int[maxVertices][maxVertices];

        for (int[] row : adjacencyMatrix) {
            Arrays.fill(row, Integer.MAX_VALUE); //Integer.MAX_VALUE represents infinity (In the strains of my shitty pc)
        }
    }

    public void addVertex(String vertex) {
        if (!vertexIndexMap.containsKey(vertex)) {
            vertexIndexMap.put(vertex, vertices.size());
            vertices.add(vertex);
        }
    }

    public void addEdge(String source, String destination, int weight) {
        addVertex(source);
        addVertex(destination);
        int sourceIndex = vertexIndexMap.get(source);
        int destinationIndex = vertexIndexMap.get(destination);
        // But ways, cause is bidirectional
        adjacencyMatrix[sourceIndex][destinationIndex] = weight;
        adjacencyMatrix[destinationIndex][sourceIndex] = weight; 
    }

    // Auto explanatory 
    public int getEdgeWeight(String source, String destination) {
        int sourceIndex = vertexIndexMap.getOrDefault(source, -1);
        int destinationIndex = vertexIndexMap.getOrDefault(destination, -1);
        if (sourceIndex == -1 || destinationIndex == -1) {
            throw new IllegalArgumentException("Invalid vertices");
        }
        return adjacencyMatrix[sourceIndex][destinationIndex];
    }

    // All this methods and below are just for the graph manager
    public String getVertex(int index) {
        return vertices.get(index);
    }

    public int getVertexIndex(String vertex) {
        return vertexIndexMap.getOrDefault(vertex, -1);
    }

    public int getVerticesCount() {
        return vertices.size();
    }
}