package com;

import com.GraphRelated.Graph;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class UVGFileReader {
    public final static String Path = "src/main/resources/";
    public static Graph CreateGraph() {
        Graph graph = new Graph(100); //Won't do magic for looking how much vertices are in the file
        try {
            BufferedReader reader = new BufferedReader(new FileReader(Path + "graph.txt"));
            String line = reader.readLine();
            while (line != null) {
                String[] parts = line.split(" ");
                graph.addEdge(parts[0], parts[1], Integer.parseInt(parts[2]));
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return graph;
    }
}