package com;

/**
 * Hello world!
 *
 */

import com.GraphRelated.Graph;
import com.GraphRelated.GraphManager;
public class App 
{
    public static void main( String[] args )
    {
        Graph graph = UVGFileReader.CreateGraph();
        while (true) {
            // I won't explain this shit, It ain't rocket science
            System.out.println("1. Calcular el camino mas corto");
            System.out.println("2. Calcular el centro del grafo");
            System.out.println("3. Restructurar Grafo");
            System.out.print("Ingrese una opcion: ");
            int option = Integer.parseInt(System.console().readLine());
            switch (option) {
                case 1:
                    System.out.print("Ingrese el vertice de origen: ");
                    String source = System.console().readLine();
                    System.out.print("Ingrese el vertice de destino: ");
                    String destination = System.console().readLine();
                    GraphManager.Floyd(graph, source, destination);
                    break;
                case 2:
                    GraphManager.Center(graph);
                    break;
                case 3:
                    System.out.print("Ingrese el vertice de origen: ");
                    String sourceVertex = System.console().readLine();
                    System.out.print("Ingrese el vertice de destino: ");
                    String destinationVertex = System.console().readLine();
                    System.out.print("Hay una parada de transito? (s/n): ");
                    String transit = System.console().readLine();
                    if (transit.equals("s")) {
                        graph.addEdge(sourceVertex, destinationVertex, -1);
                    }
                    else {
                        System.out.print("Ingrese el peso de la arista: ");
                        int weight = Integer.parseInt(System.console().readLine());
                        graph.addEdge(sourceVertex, destinationVertex, weight);
                    }
                    break;
                case 4:
                    System.exit(0);
                default:
                    System.out.println("Opcion invalida");
            }
        }
    }
}
